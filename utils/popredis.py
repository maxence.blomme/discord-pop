import os
import redis
from singleton_decorator import singleton


@singleton
class PopRedis:
    host = os.environ['REDIS_HOST']
    port = os.environ['REDIS_PORT']
    list_key = 'popl_list'

    def __init__(self):
        self.connection = redis.Redis(host=self.host, port=self.port, decode_responses=True)

    def set(self, key, value, expiration=None):
        self.connection.set(key, value, ex=expiration)

    def get(self, key):
        return self.connection.get(key)

    def delete(self, key):
        return self.connection.delete(key)

    def pop_keys(self):
        return self.connection.scan_iter('pop_*')

    def set_list(self, word_list):
        for element in word_list:
            self.connection.rpush(self.list_key, element)

    def pop_list(self):
        return self.connection.lpop(self.list_key)
