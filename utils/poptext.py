from random import randint, sample, shuffle

from utils.popredis import PopRedis


def get_random_word():
    texts = ['pop', 'lol', 'plop', 'bijour', 'bjr', 'henlo', 'mdr', 'plou', 'plouf', 'pipou', 'slt', 'bug', 'so', 'os',
             'keur', 'civ', 'bob', 'fesse', 'blob', 'mi', 'la', 'gau', 'cou', 'tap', 'tep', 'tip', 'tup', 'top', 'rap',
             'rep', 'rip', 'rup', 'rop', 'sat', 'set', 'sit', 'sut', 'sot', 'jar', 'jer', 'jir', 'jur', 'jor', 'jap',
             'jep', 'jip', 'jur', 'jop', 'las', 'les', 'lis', 'lus', 'los', 'hal', 'hel', 'hil', 'hul', 'hol', 'ham',
             'hem', 'hum', 'him', 'hom', 'etc']
    word = PopRedis().pop_list()
    if word is None:
        shuffled = [text for text in texts]
        shuffle(shuffled)
        PopRedis().set_list(shuffled)
        word = PopRedis().pop_list()
    return word


def get_pop_text():
    word = get_random_word()
    out = "*Voici le papier bulle du jour ! Saurez-vous trouver l'intrus ?*\n"
    var_i = randint(0, 9)
    var_j = randint(0, 9)
    for i in range(10):
        for j in range(10):
            if i == var_i and j == var_j:
                shuffled = word
                while shuffled == word:
                    shuffled = "".join(sample(word, len(word)))
                out += "||" + shuffled + "||"
            else:
                out += "||" + word + "||"
        out += "\n"
    return out[:-1]
