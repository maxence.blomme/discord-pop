FROM python:3.7

ENV PYTHONUNBUFFERED 1
ENV REDIS_HOST 127.0.0.1
ENV REDIS_PORT 6379

RUN mkdir /pop

WORKDIR /pop

ADD requirements.txt /pop/requirements.txt

RUN pip install -r requirements.txt

ADD . /pop/

ENTRYPOINT ["python3", "main.py"]
