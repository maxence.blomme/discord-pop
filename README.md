# Discord Pop

Papier Bulle quotidien sur Discord

## Lancement

Pour tout lancement, il sera nécessaire de se munir d'un token obtenable sur le
[site développeur de Discord](https://discord.com/developers/applications). Il est alors
conseillé de stocker ce token dans un fichier `token` disponible à la racine du projet, qu'il
faudra ignorer dans le projet Git afin d'éviter toute usurpation de bot.

### Lancement avec Docker
Une configuration Docker est fournie pour un lancement stable et simplifié du bot.

Avant de lancer le bot avec Docker, vérifiez que toutes les dépendances de paquets Python
sont correctement inscrites dans `requirements.txt`, et que le token se trouve bien dans un
fichier `token` à la racine du projet.

Une fois cela effectué, construire l'image Docker avec la commande :
```bash
docker build -t discord-pop .
```
puis lancer le conteneur en arrière-plan avec la commande :
```bash
docker run -d discord-pop
```

### Lancement en ligne de commandes
Le bot peut se lancer de deux manières différentes :
```bash
python3 main.py
python3 main.py $TOKEN
```
La première manière de lancer *nécessite* la présence du token dans un fichier `token` à la
racine du projet. La seconde permet de lancer le bot avec un token spécifique.

Il est recommandé de faire usage d'un environnement virtuel Python afin de s'assurer de la
présence constante de toutes les dépendances du projet (à l'aide de `requirements.txt`).

## Implémentation

Pour ajouter une action au bot, creer un nouveau fichier Python dans le package `actions` et y creer une
classe heritant de `actions.AbstractAction` (voir la documentation dans `action.py` pour savoir ce que fait
chaque methode, l'action `actions.Help` peut etre prise comme exemple)

Une fois la classe implementee, ajouter l'action avec `ActionList.add_action(<classe>)` au debut de
`main.py`

Le fonctionnement est similaire pour les actions aux réactions à l'aide des classes
`reaction_actions.AbstractReactionAction` et `ReactionActionList.add_action(<classe>)`

## Mode test

Le bot peut être lancé en mode test en ajoutant le flag `--test` à son lancement.

En mode test, chaque commande à envoyer au bot devra commencer par le préfixe `test`. Ainsi,
la commande `/help` devient `test/help`, `!h` devient `test!h`, etc...
