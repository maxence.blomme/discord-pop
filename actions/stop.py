from actions.action import AbstractAction
from utils.popredis import PopRedis


class Stop(AbstractAction):

    @staticmethod
    def command():
        return "pop!stop"

    @staticmethod
    def command_short():
        return "pop!st"

    @staticmethod
    def help_description():
        return "Désactiver le papier bulle pour ce serveur"

    @staticmethod
    def help_args():
        return [""]

    @staticmethod
    async def on_call(message, client):
        splitted = message.content.split()
        if len(splitted) != 1:
            await message.channel.send("*Nombre d'arguments invalide*")
            return

        PopRedis().delete("pop_" + str(message.guild.id))
        await message.channel.send("*Papier bulle désactivé pour ce serveur*")
