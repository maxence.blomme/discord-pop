from actions.action import AbstractAction
from utils.popredis import PopRedis


class Set(AbstractAction):

    @staticmethod
    def command():
        return "pop!set"

    @staticmethod
    def command_short():
        return "pop!s"

    @staticmethod
    def help_description():
        return "Définir le channel où poster le papier bulle"

    @staticmethod
    def help_args():
        return [""]

    @staticmethod
    async def on_call(message, client):
        splitted = message.content.split()
        if len(splitted) != 1:
            await message.channel.send("*Nombre d'arguments invalide*")
            return

        PopRedis().set("pop_" + str(message.guild.id), message.channel.id)
        await message.channel.send("*Channel défini pour le papier bulle quotidien !*")
